﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MISO_Website.Startup))]
namespace MISO_Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
