﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MISO_Website.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult MISOOfficersGrid()
        {
            ViewBag.Message = "MISO Officers";

            return View();
        }
        public ActionResult Gallery()
        {
            ViewBag.Message = "Gallery";
            ViewBag.Images = Directory.EnumerateFiles(Server.MapPath("~/Images/Gallary"))
                          .Select(fn => "/Images/Gallary/" + Path.GetFileName(fn));
            return View();
        }

        public ActionResult OfficerRequirement()
        {
            return View();
        }
    }
}